## Kirstie Cook
Email: kirstiea@gmail.com

I'm a software engineer and have spent the past 6 years building Ruby on Rails applications, working with a variety of stakeholders and adapting to changing project requirements in both non-profit and commercial organizations. My goal is to work for an organization that will allow me to deepen my technical knowledge while building important, useful products that make their users' lives better.


## WORK

##### Software Engineer, Senior Software Engineer, [Travelers Haven](https://www.travelershaven.com) (2017-05 — Present)
Maintaining and developing new features for the backend of the Travelers Haven Portal (THP), a Rails-based platform that connects traveling employees with fully furnished apartments. THP is used both internally and by customers in order to find housing options and manage leases. The Rails backend powers both travelershaven.com and myhavn.com, a self-serve platform that allows users to lease furnished apartments instantly.
* Responsible for all accounting-related features, including building integrations with Stripe, NetSuite, and ReadSoft for generating payables and receivables
* Automated invoice generation for all leases and brought issues found on monthly audit down from 500+ to less than 10
* Built monthly credit card auto-payment service
* Contributed to upgrade Rails 4.2 to 5.0
* Wrote services for consuming both XML and JSON external APIs to provide live inventory
* Increased test coverage of Rails API from 60% to over 70% over a large codebase
* Tools and services used: Ruby on Rails, PostgreSQL, RSpec, Sidekiq, Heroku, S3, NetSuite SuiteTalk and SuiteScript, ReadSoft
* Profiled in 'Built in Colorado' feature on how developers started coding: https://www.builtincolorado.com/2018/01/30/colorado-engineers-how-started-coding


##### Programmer Analyst I, Programmer Analyst III, Software Applications Developer I, Tarleton State University (2013-04 - 2017-04)
Built and maintained two applications, Chesapeake Bay Nutrient Trading Tool Worksheets (http://cbntt.org), and Chesapeake Bay Nutrient Trading Registry (http://registry.cbntt.org). The Worksheets tool is a Rails-based application that exposes a web interface to NTT, an internal tool that quantitatively estimates the nitrogen, phosphorus and sediment losses from crop and pasture lands. The Worksheets tool applies rules defined by environmental agencies in the Chesapeake Bay to generate credits based on conservation practices that are implemented by land owners. These credits are able to be transferred to the Registry tool, another Rails-based application, which provides a marketplace for stakeholders to buy and sell their nutrient credits.
* Upgraded Chesapeake Bay Nutrient Trading Registry from Rails 3.2 to 4.0
* Integrated public-facing web services with internal tools using a private XML API
* Migrated web services off of local servers and onto cloud servers (Rackspace and AWS S3)
* Gathered software requirements from stakeholders and wrote specification documents
* Wrote monthly progress reports on the development of the Chesapeake Bay Nutrient Trading applications in order to continue receiving project funds from granting agencies
* Tools and services used: Ruby on Rails, RSpec, MySQL, Rackspace, S3


## EDUCATION

##### Virginia Tech (2006-2011)
Bachelor of Science, Food Science and Technology


## RECOGNITION

##### Travelers Haven Employee of the Month (Feb 2018)
